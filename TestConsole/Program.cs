﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Type type = null;

            if (args.Length > 0)
                type = GetTestClassType(args[0]);

            while (type == null)
            {
                type = SelectTypeFromList();
            }

            ITestClass test = InitTestClass(type);

            do
            {
                Console.WriteLine($"Running {test.GetType().Name}:");
                test.Run();
                Console.WriteLine("Run complete, press <ESCAPE> to end or any other key to repeat");
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }

        private static ITestClass InitTestClass(Type type)
        {
            var myCtor = type.GetConstructor(new Type[] { });

            ITestClass test = (ITestClass)myCtor.Invoke(null);
            return test;
        }

        private static Type GetTestClassType(string className)
        {
            var type = Assembly.GetExecutingAssembly().GetType($"TestConsole.{className}", false, true);


            return type;
        }

        private static Type SelectTypeFromList()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(x => typeof(ITestClass).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .ToList();

            types.Sort((a, b) => a.Name.CompareTo(b.Name));

            Console.WriteLine("Available tests:");
            for (int i = 0; i < types.Count; i++)
            {
                var type = types[i];
                Console.WriteLine($"{i + 1}. {type.Name}");
            }
            Console.Write("Select a test by name or number: ");
            var className = Console.ReadLine();

            if (int.TryParse(className, out int numSelected))
            {
                if (numSelected > 0 && numSelected <= types.Count)
                {
                    return types[numSelected - 1];
                }

                return null;
            }

            return GetTestClassType(className);
        }
    }

}
